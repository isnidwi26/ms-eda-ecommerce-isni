﻿using LookUp.Domain;

namespace LookUp.GraphQL.Scema.Mutations
{
    public class AttributeTypeInput
    {
        public AttributeTypeEnum Type { get; set; }
        public string Unit { get; set; }

    }
}
