﻿namespace LookUp.GraphQL.Scema.Mutations
{
    public class CurrenciesTypeInput
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Symbol { get; set; }
    }
}
