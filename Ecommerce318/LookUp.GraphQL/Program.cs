using Framework.Kafka;
using LookUp.Domain;
using LookUp.Domain.MapProfile;
using LookUp.Domain.Repositories;
using LookUp.Domain.Services;
using LookUp.GraphQL.Scema.Mutations;
using LookUp.GraphQL.Scema.Queries;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDomainContext(options =>
{
    var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json",
                optional: true, reloadOnChange: true);

    options
        .UseSqlServer(builder.Build()
        .GetSection("ConnectionStrings")
        .GetSection("LookUp_Db_Conn").Value);

    options
        .EnableSensitiveDataLogging(false)
        .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
});



builder.Services.AddControllers();
builder.Services.AddAutoMapper(config =>
{
    config.AddProfile<EntityToDtoProfile>();
});

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddKafkaProducer();

builder.Services.AddAuthentication(opt =>
{
    opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
})
    .AddJwtBearer("Bearer", opt =>
    {
        var configuration = builder.Configuration;
        opt.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuer = true,
            ValidateAudience = true,
            ValidateLifetime = true,
            ValidateIssuerSigningKey = true,
            ValidIssuer = configuration["JWT:ValidIssuer"],
            ValidAudience = configuration["JWT:ValidAudience"],
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:Secret"]))
        };
        opt.Events = new JwtBearerEvents
        {
            OnChallenge = context =>
            {
                context.Response.OnStarting(async () =>
                {
                    await context.Response.WriteAsync("Account not authorized");
                });
                return Task.CompletedTask;
            },
            OnForbidden = context =>
            {
                context.Response.OnStarting(async () =>
                {
                    await context.Response.WriteAsync("Account forbidden");
                });
                return Task.CompletedTask;
            }

        };
    });
builder.Services
    .AddScoped<AttributeQuery>()
    .AddScoped<CurrenciesQuery>()
    .AddScoped<AttributeMutation>()
    .AddScoped<CurrenciesMutation>()
    .AddScoped<IAttributeService, AttributeService>()
    .AddScoped<IAttributeRepository, AttributeRepository>()
    .AddScoped<ICurrenciesService, CurrenciesService>()
    .AddScoped<ICurrenciesRepository, CurrenciesRepository>()
    .AddGraphQLServer()
    .AddQueryType(q => q.Name("Query"))
    .AddType<AttributeQuery>()
    .AddType<CurrenciesQuery>()
    .AddMutationType(m => m.Name("Mutation"))
    .AddType<AttributeMutation>()
    .AddType<CurrenciesMutation>()
    .AddAuthorization();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.UseRouting();

app.UseAuthentication();

app.MapControllers();

app.MapGraphQL();

app.Run();
