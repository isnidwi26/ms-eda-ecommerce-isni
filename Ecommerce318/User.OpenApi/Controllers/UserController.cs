﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using User.Domain;
using User.Domain.Dtos;
using User.Domain.Services;

namespace User.OpenApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _service;
        private readonly ILogger<UserController> _logger;

        public UserController(IUserService service, ILogger<UserController> logger)
        {
            _service = service;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return Ok(await _service.All());
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] UserDto payload, CancellationToken cancellationToken)
        {
            try
            {
                var dto = await _service.AddUser(payload);
                if (dto != null)
                    return Ok(dto);
            }
            catch (OperationCanceledException oce)
                when (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(oce.Message);
            }
            return BadRequest();
        }

        [HttpPut]
        public async Task<IActionResult> Put(Guid id, [FromBody] UserDto payload, CancellationToken cancellationToken)
        {
            try
            {
                if(payload != null)
                {
                    payload.Id = id;
                    var isUpdated = await _service.UpdateUser(payload);
                    if (isUpdated)
                        return Ok(isUpdated);
                }
            }
            catch (OperationCanceledException oce)
                when (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(oce.Message);
            }
            return NoContent();
        }

        [HttpPut("Status")]
        public async Task<IActionResult> Put(Guid id, UserStatusEnum status, CancellationToken cancellationToken)
        {
            try
            {
                var isUpdated = await _service.UpdateStatus(id, status);
                if (isUpdated)
                    return Ok(isUpdated);
            }
            catch (OperationCanceledException oce)
                when (cancellationToken.IsCancellationRequested)
            {
                _logger.LogWarning(oce.Message);
            }
            return NoContent();
        }
    }
}
