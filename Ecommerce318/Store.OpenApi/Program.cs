using Store.Domain;
using Microsoft.EntityFrameworkCore;
using Store.Domain.MapProfile;
using Store.Domain.Validations;
using FluentValidation;
using Framework.Kafka;
using Framework.Core.Events;
using Store.Domain.Services;
using Store.Domain.Repository;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDomainContext(options =>
{
    var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

    options.UseSqlServer(builder.Build().GetSection("ConnectionStrings").GetSection("Store_Db_Conn").Value);

    options
        .EnableSensitiveDataLogging(false)
        .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
});

builder.Services
    .AddScoped<ICategoriesRepository, CategoriesRepository>()
    .AddScoped<ICategoriesService, CategoriesService>();

builder.Services.AddValidatorsFromAssemblyContaining<CategoryValidator>();

builder.Services.AddControllers();

builder.Services.AddStore();

builder.Services.AddAutoMapper(config =>
{
    config.AddProfile<EntityToDtoProfile>();
});
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddEventBus();
builder.Services.AddKafkaProducerAndConsumer();


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();


//var builder = WebApplication.CreateBuilder(args);

//// Add services to the container.

//builder.Services.AddDomainContext(options =>
//{
//    var builder = new ConfigurationBuilder()
//                .SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json",
//                optional: true, reloadOnChange: true);

//    options
//        .UseSqlServer(builder.Build()
//        .GetSection("ConnectionStrings")
//        .GetSection("Store_Db_Conn").Value);

//    options
//        .EnableSensitiveDataLogging(false)
//        .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
//});

//builder.Services
//    .AddScoped<ICategoriesRepository, CategoriesRepository>()
//    .AddScoped<ICategoriesService, CategoriesService>()
//    .AddScoped<IProductService, ProductService>()
//    .AddScoped<IProductRepository, ProductRepository>();

//builder.Services.AddValidatorsFromAssemblyContaining<CategoryValidator>();
//builder.Services.AddValidatorsFromAssemblyContaining<ProductValidator>();
//builder.Services.AddControllers();
//builder.Services.AddStore();
//builder.Services.AddAutoMapper(config =>
//{
//    config.AddProfile<EntityToDtoProfile>();
//});
//// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
//builder.Services.AddEndpointsApiExplorer();
//builder.Services.AddSwaggerGen();

//builder.Services.AddEventBus();
//builder.Services.AddKafkaProducerAndConsumer();
////builder.Services.AddKafkaProducer();

//var app = builder.Build();

//// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
//    app.UseSwagger();
//    app.UseSwaggerUI();
//}

//app.UseHttpsRedirection();

//app.UseAuthorization();

//app.MapControllers();

//app.Run();
