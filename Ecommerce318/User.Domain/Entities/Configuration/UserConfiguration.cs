﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace User.Domain.Entities.Configuration
{
    public class UserConfiguration : IEntityTypeConfiguration<UsersEntity>
    {
        public void Configure(EntityTypeBuilder<UsersEntity> builder)
        {
            builder.ToTable("Users");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).IsRequired();
            builder.Property(e => e.UserName).IsRequired();
            builder.Property(e => e.Password).HasMaxLength(8).IsRequired();
            builder.Property(e => e.FirstName).HasMaxLength(15).IsRequired();
            builder.Property(e => e.LastName).HasMaxLength(15).IsRequired();
            builder.Property(e => e.Email).HasMaxLength(30).IsRequired();
            builder.Property(e => e.Type).IsRequired();
            builder.Property(e => e.Status).IsRequired();
        }
    }
}
