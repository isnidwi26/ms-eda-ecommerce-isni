﻿using AutoMapper;
using Framework.Auth;
using Framework.Core.Events;
using Framework.Core.Events.Externals;
using User.Domain.Dtos;
using User.Domain.Entities;
using User.Domain.EventEnvelopes.User;
using User.Domain.Repositories;

namespace User.Domain.Services
{
    public interface IUserService
    {
        Task<IEnumerable<UserDto>> All();
        Task<UserDto> GetUserById(Guid id);
        Task<UserDto> AddUser(UserDto dto);
        Task<bool> UpdateUser(UserDto dto);
        Task<bool> UpdateStatus(Guid id, UserStatusEnum status);
        Task<LoginDto> Login(string userName, string password);

    }

    public class UserService : IUserService
    {
        private IUserRepository _repository;
        private readonly IMapper _mapper;
        private readonly IExternalEventProducer _externalEventProducer;

        public UserService(IUserRepository repository, IMapper mapper, IExternalEventProducer externalEventProducer)
        {
            _repository = repository;
            _mapper = mapper;
            _externalEventProducer = externalEventProducer;
        }

        public async Task<UserDto> AddUser(UserDto dto)
        {
            if (dto != null)
            {
                dto.Status = UserStatusEnum.Inactive;
                dto.Password = Encryption.HashSha256(dto.Password);
                var dtoToEntity = _mapper.Map<UsersEntity>(dto);
                var entity = await _repository.Add(dtoToEntity);
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                {
                    var externalEvent = new EventEnvelope<UserCreated>
                        (
                            UserCreated.Create(
                                entity.Id,
                                entity.FirstName,
                                entity.LastName,
                                entity.Email
                                )
                        );
                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                    return _mapper.Map<UserDto>(entity);
                }
            }
            return new UserDto();
        }

        public async Task<IEnumerable<UserDto>> All()
        {
            return _mapper.Map<IEnumerable<UserDto>>(await _repository.GetAll());
        }

        public async Task<UserDto> GetUserById(Guid id)
        {
            if (id != Guid.Empty)
            {
                var result = await _repository.GetById(id);
                if (result != null)
                    return _mapper.Map<UserDto>(result);
            }
            return null;
        }
        public async Task<bool> UpdateUser(UserDto dto)
        {
            if (dto != null)
            {
                var users = await _repository.GetById(dto.Id);
                dto.Status = users.Status;
                dto.Password = Encryption.HashSha256(users.Password);
                if (users != null)
                {
                    var entity = await _repository.Update(_mapper.Map<UsersEntity>(dto));
                    var result = await _repository.SaveChangesAsync();

                    if (result > 0)
                    {
                        var externalEvent = new EventEnvelope<UserUpdate>
                            (
                                UserUpdate.EditUser(
                                        entity.Id,
                                        entity.FirstName,
                                        entity.LastName
                                    )
                            );
                        await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                        return true;
                    }
                }
            }
            return false;
        }

        public async Task<bool> UpdateStatus(Guid id, UserStatusEnum status)
        {
            var users = await _repository.GetById(id);
            if (users != null)
            {
                users.Status = status;
                var entity = await _repository.Update(users);
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                {
                    return true;
                }
            }
            return false;
        }

        public async Task<LoginDto> Login(string userName, string password)
        {
            var entity = await _repository.Login(userName, password);
            if (entity != null)
            {
                LoginDto dto = _mapper.Map<LoginDto>(entity);
                dto.Roles.Add(entity.Type.ToString());
                return dto;
            }
            return null;
        }
    }
}
