﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace User.Domain.EventEnvelopes.User
{
    public record UserUpdate
        (
            Guid id,
            string FirstName,
            string LastName
        )
    {
        public static UserUpdate EditUser
            (
                Guid id,
                string firstName,
                string lastName
            ) => new(id, firstName, lastName);
    }
}
