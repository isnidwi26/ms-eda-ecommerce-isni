﻿using Payment.Domain.Dtos;
using Payment.Domain.Services;

namespace Payment.GraphQL.Scema.Mutations
{
    [ExtendObjectType("Mutation")]
    public class PaymentMutation
    {
        private readonly IPaymentService _paymentService;

        public PaymentMutation(IPaymentService paymentService)
        {
            _paymentService = paymentService;
        }

        public async Task<CartDto> Payment(Guid cartId, decimal amount)
        {
            return await _paymentService.Paying(cartId, amount);
        }
    }
}
