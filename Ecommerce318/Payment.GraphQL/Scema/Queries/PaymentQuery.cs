﻿using Payment.Domain.Dtos;
using Payment.Domain.Services;

namespace Payment.GraphQL.Scema.Queries
{
    [ExtendObjectType("Query")]
    public class PaymentQuery
    {
        private readonly IPaymentService _paymentService;

        public PaymentQuery(IPaymentService paymentService)
        {
            _paymentService = paymentService;
        }

        public async Task<IEnumerable<CartDto>> GetAllAsync()
        {
            return await _paymentService.All();
        }
    }
}
