﻿using HotChocolate.Authorization;
using Store.Domain;
using Store.Domain.Dtos;
using Store.Domain.Services;

namespace Store.GraphQL.Scema.Mutations
{
    [ExtendObjectType("Mutation")]
    public class ProductMutation
    {
        private readonly IProductService _service;

        public ProductMutation(IProductService service)
        {
            _service = service;
        }
        [Authorize(Roles = new[] { "admin" })]
        public async Task<ProductDto> AddProductAsync(ProductTypeInput product)
        {
            ProductDto dto = new ProductDto();
            dto.AttributeId = product.AttributeId;
            dto.CategoryId = product.CategoryId;
            dto.Sku = product.Sku;
            dto.Name = product.Name;
            dto.Description = product.Description;
            dto.Price = product.Price;
            dto.Volume = product.Volume;
            dto.Sold = product.Sold;
            dto.Stock = product.Stock;

            return await _service.AddProduct(dto);
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<ProductDto> EditProductAsync(Guid id, ProductTypeInput product)
        {
            ProductDto dto = new ProductDto();
            dto.Id = id;
            dto.AttributeId = product.AttributeId;
            dto.CategoryId = product.CategoryId;
            dto.Sku = product.Sku;
            dto.Name = product.Name;
            dto.Description = product.Description;
            dto.Price = product.Price;
            dto.Volume = product.Volume;
            dto.Sold = product.Sold;
            dto.Stock = product.Stock;

            var result = await _service.UpdateProduct(dto);
            if (!result)
            {
                throw new GraphQLException(new Error("Product not found, 404"));
            }

            return dto;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<ProductDto> EditStatusProductAsync(Guid id, StoreStatusEnum status)
        {
            var result = await _service.UpdateStatus(id, status);
            if (!result)
            {
                throw new GraphQLException(new Error("Product not found, 404"));
            }
            return await _service.GetProductById(id);
        }
    }
}
