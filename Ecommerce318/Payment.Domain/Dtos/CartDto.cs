﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Payment.Domain.Dtos
{
    public class CartDto
    {
        public Guid Id { get; set; }
        public Guid CustomerId { get; set; }
        public decimal Total { get; set; }
        public decimal Pay { get; set; }
        public PaymentStatusEnum PaymentStatus { get; set; }
        public CartStatusEnum Status { get; set; } 
        public DateTime Modified { get; internal set; } = DateTime.Now;
        public UserDto User { get; set; }
    }
}
