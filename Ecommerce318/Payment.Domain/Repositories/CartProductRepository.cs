﻿

using Microsoft.EntityFrameworkCore;
using Payment.Domain.Entities;

namespace Payment.Domain.Repositories
{
    public interface ICartProductRepository
    {
        Task<IEnumerable<CartProductEntity>> GetCartById(Guid id);
    }

    public class CartProductRepository : ICartProductRepository
    {
        protected readonly PaymentDbContext _context;

        public CartProductRepository(PaymentDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }
        public async Task<IEnumerable<CartProductEntity>> GetCartById(Guid id)
        {
            return await _context.Set<CartProductEntity>().Where(o => o.CartId == id).ToListAsync();
        }
    }
}
