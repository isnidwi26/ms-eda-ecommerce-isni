﻿using Framework.Core.Events;
using Payment.Domain.Entities;

namespace Payment.Domain.Projections
{
    public record UserCreated(
        Guid Id,
        string FirstName,
        string LastName,
        string Email
    );

    public class UserProjection
    {

        public static bool Handle(EventEnvelope<UserCreated> eventEnvelope)
        {
            var (id, firstName, lastName, email) = eventEnvelope.Data;
            using (var context = new PaymentDbContext(PaymentDbContext.OnConfigure()))
            {
                UserEntity entity = new UserEntity()
                {
                    Id = (Guid)id,
                    FirstName = firstName,
                    LastName = lastName,
                    Email = email
                };

                context.Users.Add(entity);
                context.SaveChanges();
            }
            return true;
        }
    }
}
