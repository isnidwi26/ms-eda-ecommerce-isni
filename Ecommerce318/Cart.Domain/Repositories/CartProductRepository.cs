﻿using Microsoft.EntityFrameworkCore;
using Cart.Domain.Entities;

namespace Cart.Domain.Repositories
{
    public interface ICartProductRepository
    {
        Task<IEnumerable<CartProductEntity>> GetAll();
        Task<CartProductEntity> GetById(Guid id);
        Task <IEnumerable<CartProductEntity>> GetByCartId(Guid id);
        Task <CartProductEntity> GetByCartId(Guid id, Guid productId);
        Task<CartProductEntity> Add(CartProductEntity entity);
        Task<CartProductEntity> Update(CartProductEntity entity);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }

    public class CartProductsRepository : ICartProductRepository
    {
        protected readonly CartDbContext _context;
        public CartProductsRepository(CartDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }
        public async Task<CartProductEntity> Add(CartProductEntity entity)
        {
            _context.Set<CartProductEntity>().AddAsync(entity);
            return entity;
        }
       
        public async Task<IEnumerable<CartProductEntity>> GetAll()
        {
            return await _context.Set<CartProductEntity>().ToListAsync();
        }

        public async Task<CartProductEntity> GetById(Guid id)
        {
            return await _context.Set<CartProductEntity>().FindAsync(id);
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task<CartProductEntity> Update(CartProductEntity entity)
        {
            _context.Set<CartProductEntity>().Update(entity);
            return entity;
        }

        public async Task<IEnumerable<CartProductEntity>> GetByCartId(Guid id)
        {
            return await _context.Set<CartProductEntity>().Where(o => o.CartId == id).ToListAsync();
        }

        public async Task<CartProductEntity> GetByCartId(Guid id, Guid productId)
        {
            return await _context.Set<CartProductEntity>().Where(o => o.CartId == id && o.ProductId == productId).FirstOrDefaultAsync();
        }
        public virtual void Dispose(bool disposing)
        {
            if (disposing)
                _context.Dispose();
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

    }
}
