﻿

namespace Cart.Domain.EvenEnvelopes.CartProduct
{
    public record CartProductUpdate(
        Guid Id,
        Guid CartId,
        int Quantity
        )
    {
        public static CartProductUpdate Create(
            Guid id,
            Guid cartId,
            int quantity
            ) => new(id, cartId, quantity);
    }
}
