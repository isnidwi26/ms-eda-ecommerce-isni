﻿

namespace Cart.Domain.EvenEnvelopes.Cart
{
    public record CartCreated(
        Guid Id,
        Guid CustomerId,
        CartStatusEnum Status)
    {
        public static CartCreated Create(
            Guid id,
            Guid customerId,
            CartStatusEnum status
            ) => new(id, customerId, status);
    }
}
