﻿
using System.ComponentModel.DataAnnotations.Schema;


namespace Cart.Domain.Entities
{
    public class CartsEntity
    {
        public Guid Id { get; set; }
        public Guid CustomerId { get; set; }
        public CartStatusEnum Status { get; set; } = default!;
        public DateTime Modified { get; internal set; } = DateTime.Now;

        [ForeignKey("CustomerId")]
        public virtual UsersEntity User { get; set; }
        public virtual ICollection<CartProductEntity> CartProducts { get; set; }   

    }
}
