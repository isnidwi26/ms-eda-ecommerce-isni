﻿using Cart.Domain.Entities;
using Framework.Core.Events;


namespace Cart.Domain.Projections
{
    public record ProductCreated(
        Guid? Id,
        decimal Price,
        int Stock,
        StoreStatusEnum Status
    );

    public class ProductProjection
    {
        public static bool Handle(EventEnvelope<ProductCreated> eventEnvelope)
        {
            var (id, price, stock, status) = eventEnvelope.Data;
            using (var context = new CartDbContext(CartDbContext.OnConfigure()))
            {
                ProductEntity entity = new ProductEntity()
                {
                    Id = (Guid)id,
                    Price = price,
                    Stock = stock,
                    Status = status
                };

                context.Products.Add(entity);
                context.SaveChanges();
            }
            return true;
        }
    }
}
