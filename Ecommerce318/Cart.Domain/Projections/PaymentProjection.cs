﻿using Cart.Domain.Entities;
using Framework.Core.Events;


namespace Cart.Domain.Projections
{
    public record PaymentCreated(
        Guid CartId,
        List<CartProductItem> CartProducts,
        CartStatusEnum Status
    );

    public class CartProductItem
    {
        public Guid ProductId { get; set; }
        public int Stock { get; set; }
        public int Quantity { get; set; }
    }

    public class PaymentProjection
    {
        public static bool Handle(EventEnvelope<PaymentCreated> eventEnvelope)
        {
            var (cartId, cartProducts, status) = eventEnvelope.Data;
            using (var context = new CartDbContext(CartDbContext.OnConfigure()))
            {
                foreach (var cp in cartProducts)
                {
                    var product = context.Products.Where(o => o.Id == cp.ProductId).FirstOrDefault();
                    if (product != null)
                    {
                        product.Stock = cp.Stock;
                        context.Products.Update(product);
                    }
                }

                //var cart = context.Carts.Where(o => o.Id == cartId).FirstOrDefault();
                //if (cart != null)
                //{
                //    cart.Status = status;
                //    context.Carts.Update(cart);
                //}

                CartsEntity entity = context.Set<CartsEntity>().Find(cartId);
                entity.Status = status;
                context.SaveChanges();
            }

            return true;
        }
    }
}
