﻿

using FluentValidation;
using Store.Domain.Dtos;

namespace Store.Domain.Validations
{
    public class ProductValidator : AbstractValidator<ProductDto>
    {
        public ProductValidator()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("Name cannot empty");
            RuleFor(x => x.Description).MinimumLength(5).WithMessage("Description minimum 5 chars");
            RuleFor(x => x.Sku).MinimumLength(3).WithMessage("SKU minimum 3 chars").Matches("^[0-9]*$").WithMessage("SKU must consist of numbers");
            
        }
    }
}
