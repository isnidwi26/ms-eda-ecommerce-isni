﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Domain.EventEnvelope.Product
{
    public record ProductUpdated
    (
        Guid Id,
        Guid CategoryId,
        Guid AttributeId,
        string Sku,
        string Name,
        string Description,
        decimal Price,
        decimal Volume,
        int Sold,
        int Stock
        
    )
    {
        public static ProductUpdated UpdateData(
            Guid id,
            Guid categoryId,
            Guid attributeId,
            string sku,
            string name,
            string description,
            decimal price,
            decimal volume,
            int sold,
            int stock
            ) => new(id, categoryId, attributeId, sku, name, description, price, volume, sold, stock);
    }
}
