﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Domain.EventEnvelope.Product
{
    public record ProductCreated
    (
        Guid? Id,
        decimal Price,
        int Stock,
        StoreStatusEnum Status
    )
    {
        public static ProductCreated Create(
            Guid id,
            decimal price,
            int stock,
            StoreStatusEnum status
            ) => new(id,  price, stock, status);
    }
}

//store consume attribute from lookup
