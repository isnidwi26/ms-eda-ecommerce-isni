﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LookUp.Domain.EventEnvelope.Attribute
{
    public record AttributeStatusChanged(
        Guid Id,
        LookUpStatusEnum Status
        )
    {
        public static AttributeStatusChanged UpdateStatus
        (
            Guid id,
            LookUpStatusEnum status
        ) => new(id, status);
    }
}
