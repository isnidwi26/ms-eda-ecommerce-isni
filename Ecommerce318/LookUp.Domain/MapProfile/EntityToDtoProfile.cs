﻿using AutoMapper;
using LookUp.Domain.Dtos;
using LookUp.Domain.Entities;

namespace LookUp.Domain.MapProfile
{
    public class EntityToDtoProfile : Profile
    {
        public EntityToDtoProfile() : base("Entity to Dto Profile")
        {
            CreateMap<AttributeEntity, AttributeDto>();
            CreateMap<AttributeDto, AttributeEntity>();


            CreateMap<CurrenciesEntity, CurrenciesDto>();
            CreateMap<CurrenciesDto, CurrenciesEntity>();
        }
    }
}
